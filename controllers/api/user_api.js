"use strict";

const status = require('../../utils/status')
const model = require('../../models/user')

const user = new model.UserActions()
const hash = require('../../services/hash')

const emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i

class UserApi
{
    register(req, res)
    {
        if(req.body.nick && req.body.password && req.body.email)
        {
            let newUser = new model.user(
            {
                email: req.body.email,
                nick: req.body.nick,
                name: req.body.name,
                last_name: req.last_name,
                password : req.body.password,
                lastLogin: Date.now(),
                avatar: req.body.avatar
            
            })            
            
            /*check for incomplete data: missing email, bad email, missing nick, or missing password?*/
            
            if((newUser.email).includes (' ') ||
                (newUser.nick).includes (' ') ||
                !emailRegex.test(newUser.email)
            ) return res.status(status.bad.code).send({error:status.bad.msg});
            
            user.registerUser(newUser, (ok, msg) => 
            {
                if(ok) 
                    return res.status(status.ok.code).send({
                        message:msg.message, 
                        token: hash.createToken({
                            id: newUser._id,
                            nick: newUser.email,
                            email: newUser.email                            
                        })                        
                    });                              
                              
                return res.status(status.bad.code).send(msg);
                
            }) 
        } else return res.status(status.bad.code).send({error:status.bad.msg});      
    }
    
    login(req, res)
    {
       if((!req.body.email && !req.body.nick) || !req.body.password)
           return res.status(status.bad.code).send({error:status.bad.msg});      
        
        user.passUser(req.body, (ok, data) =>
        {
            if(ok)
            {
                user.getUser({$or:[{email: req.body.email}, {nick: req.body.nick}]}, (ok, msg)=>
                {
                    if(ok)
                    {
                        req.user = msg;
                        let token = hash.createToken(msg);
                        msg.lastLogin = Date.now()
                        
                        user.update(msg, (ok, msg) =>
                        {
                            console.log(ok, msg)
                        });
                        
                        return res.status(status.ok.code).send({msg: msg, token: token})
                        
                    }else return res.status(status.not_found.code).send({error: status.not_found.msg}); 
                }); 
            }else return res.status(status.unauthorized.code).send({error: status.unauthorized.msg}); 
            
        });
    }
}

module.exports = UserApi
