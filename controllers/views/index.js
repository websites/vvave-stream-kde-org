"use strict"

class Index
{
    home(req, res)
    {
        res.status(200).render("index", {color: "#111"})
    }
    
    api(re, res)
    {
        res.status(200).render("api", {color: "#dedede"})
    }
}

module.exports = Index
