"use strict"

const base = require('./base')

/*CONTROLLERS*/
const ctrl = require('../controllers/controller') 

/*OBJS*/
const index = new ctrl.BabeIt()

/*ROUTES*/
base.get('/', index.home)
base.get('/home', index.home)
base.get('/api', index.api)


